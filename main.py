"""Example program"""

print("EMATE")
print("LEPIEJ SIE WITAC NIZ ZEGNAC")

"""Python part"""

integer_variable: int = 2
float_variable: float = 1.0
string_variable: str = "String"
boolean_variable: bool = True

print(int("123"))
print(int(3.14))
print(int(False))

print(bool("A"))
print(bool(None))
print(bool("False"))
print(bool(""))

print(str(123123))
print(str(True))
print(str(3.12412))

print(float(4))


class Car:
    pass


cabriolet = Car()
print(cabriolet)


class Cabriolet(Car):
    pass


class Truck(Car):
    pass


cabriolet = Cabriolet()
truck = Truck()

print(cabriolet, truck)
print(bool(cabriolet))

